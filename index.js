var httpProxy = require('http-proxy');

const targetUrl = 'https://api-job-qa.ci.mcf.sh';

const proxy = httpProxy.createProxyServer({
  target: targetUrl,
  secure: false,
  changeOrigin: true,
  cookieDomainRewrite: {
    '*': '',
  },
});

proxy.on('proxyRes', function (proxyRes, req, res, options) {
  const sc = proxyRes.headers['set-cookie'];
  if (Array.isArray(sc)) {
    proxyRes.headers['set-cookie'] = sc.map((sc) => {
      return sc
        .split(';')
        .filter((v) => v.trim().toLowerCase() !== 'secure')
        .join('; ');
    });
  }
});

proxy.listen(5173);
