FROM public.ecr.aws/docker/library/node:18-alpine
WORKDIR /server
COPY package*.json ./
RUN npm install
COPY index.js index.js
USER root
RUN chown 1000:1000 -R ./
USER 1000
ENTRYPOINT [ "node", "index.js" ]
EXPOSE 5173
